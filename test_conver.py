import ffmpy
import sys,os
from flask import Flask,jsonify,request
#from gevent.pywsgi import WSGIServer
from flask_sqlalchemy import SQLAlchemy
import requests
import json
import os
from threading import Thread
import threading
import time
import uuid
import ffmpy
from api_orm import *
import subprocess
import logging
from sqlalchemy import and_
from subprocess import PIPE
import socket
    
UDP_IP = "163.18.26.108"
UDP_PORT = 7602
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
logging.basicConfig(level=logging.DEBUG, filename='./ffmpylog.txt', filemode='w', format=FORMAT)

logging.debug('debug message')
logging.info('info message')
logging.warning('warning message')
logging.error('error message')
logging.critical('critical message')

Session = sessionmaker(bind=DBlink)
session = Session()

def requests_api():
    statu = False
    res = ''
    while statu == False :
          headers = {'Content-type': 'application/json'}
          r = requests.post('https://ai-interview.54ucl.com:3004/v2/Session/RequestJobID', headers=headers)
          res = json.loads(r.text)['JobID']
          if res>1:
             print(res)
             statu = True
             return res
          time.sleep(2)
    #return r.text

def upload_api(sessionid,jobid):

    statu = False
    r = ''
    while statu == False :
          '''if ( os.path.isfile(('uploadfile/'+sessionid+'.mkv'))==False ):
              message = {
                 "Server": "216_nfs",
                 "API": "ConverFile",
                 "log": sessionid+' conver failed',
              }
              sock = socket.socket(socket.AF_INET, # Internet
                  socket.SOCK_DGRAM) # UDP
              sock.sendto(json.dumps(message).encode('utf-8'), (UDP_IP, UDP_PORT))'''
 
          headers = {'SessionID':sessionid,'JobID':str(jobid),'DataSize':str(os.path.getsize('uploadfile/'+sessionid+'.mkv'))}
    #converfile(Files)

          files = {'File':open('uploadfile/'+sessionid+'.mkv','rb')}
          r = requests.post('https://ai-interview.54ucl.com:3004/v2/Session/UploadFiles', headers=headers,files=files)
          if r.text != None and  len(r.text)>=1 :
             print(r.text)
             statu = True
             return r.text
          time.sleep(3)

def check_api(jobid,SessionID):

    statu = False
    r = ''
    while statu == False :

          data = {'JobID':jobid}
          json1 = json.dumps(data)
          r = requests.post('https://ai-interview.54ucl.com:3004/v2/Session/CheckFiles', headers={'Content-Type': 'application/json'},data=json1)
          if r.text != None and  len(r.text)>=1 :
             print(r.text)
             AllData = session.query(Proxy).filter(Proxy.SessionID==SessionID).order_by(Proxy.id).first()
             print(AllData.upload)
             AllData.upload=1
             session.add(AllData)
             session.commit()
             statu = True
             return r.text
          time.sleep(3)
    #return r.text


def converfile(filename,SessionID):
    try:
       ff = ffmpy.FFmpeg(
           inputs = {filename:'-y'},
           outputs = {'uploadfile/'+ SessionID+'.mkv':'-vcodec copy -acodec copy -y'}
       )
       ff.run(stderr=PIPE)
       AllData = session.query(Proxy).filter(Proxy.SessionID==SessionID).order_by(Proxy.id).first()
       print(AllData.conver)
       AllData.conver=2
       session.add(AllData)
       session.commit()
    except :

       message = {
           "Server": "216_nfs",
           "Action": "ConverFile",
           "log": str(sys.exc_info()[1]),
       }
       sock = socket.socket(socket.AF_INET, # Internet
                  socket.SOCK_DGRAM) # UDP
       sock.sendto(json.dumps(message).encode('utf-8'), (UDP_IP, UDP_PORT))
       #print(e)
       pass


if __name__ == '__main__':
    path = sys.argv[1]
    sessionid = sys.argv[2]
    print(path)
    converfile(path,sessionid)
    Api_id = requests_api()
    #SessionID = non_upload.SessionID
    #files = non_upload.files
    #datasize = os.path.getsize(files)
    time.sleep(3)
    upload_api(sessionid,Api_id)
    time.sleep(2)
    check_api(Api_id,sessionid)
    #non_upload.upload=1
    #session.add(non_upload)
    #session.commit()

