from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, Index,VARCHAR,BOOLEAN,VARBINARY,Float,TEXT
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import create_engine,and_, or_
from sqlalchemy.pool import NullPool
import os
import shutil

DBInfo = 'mysql+pymysql://root:c217db@163.18.26.147/i2aFor1111?charset=utf8mb4'
#DBlink = create_engine(DBInfo, encoding='utf-8', pool_timeout=20, pool_recycle=299,pool_size=100)
DBlink = create_engine(DBInfo,poolclass=NullPool)
Base = declarative_base()

class Proxy(Base):
    __tablename__ = 'Proxy'
    id = Column(Integer, primary_key=True,autoincrement=True)
    files = Column(VARCHAR(80))
    upload = Column(Integer)
    DataSize = Column(VARCHAR(200))
    SessionID = Column(VARCHAR(200))
    conver = Column(Integer)

def init_db():
    Base.metadata.create_all(DBlink)

def drop_db():
    Base.metadata.drop_all(DBlink)
#drop_db()
init_db()
