from flask import Flask,jsonify,request
#from gevent.pywsgi import WSGIServer
from flask_sqlalchemy import SQLAlchemy
import requests
import json
import os
from threading import Thread
import threading
import time
import uuid
from api_orm import *
import ffmpy
import logging
import subprocess
from sqlalchemy import and_
import socket

FORMAT = '%(asctime)s %(levelname)s: %(message)s'
logging.basicConfig(level=logging.DEBUG, filename='./myLog.txt', filemode='w', format=FORMAT)

logging.debug('debug message')
logging.info('info message')
logging.warning('warning message')
logging.error('error message')
logging.critical('critical message')

UDP_IP = "163.18.26.108"
UDP_PORT = 7602

app = Flask(__name__)

Session = sessionmaker(bind=DBlink)
session = Session()

def requests_api():
    headers = {'Content-type': 'application/json'}
    r = requests.post('https://ai-interview.54ucl.com:3004/v2/Session/RequestJobID', headers=headers)
    print(r.text)
    return r.text

def upload_api(sessionid,jobid):
    
    headers = {'SessionID':sessionid,'JobID':str(jobid),'DataSize':str(os.path.getsize('uploadfile/'+sessionid+'.mkv'))}
    #converfile(Files)

    files = {'File':open('uploadfile/'+sessionid+'.mkv','rb')}
    r = requests.post('https://ai-interview.54ucl.com:3004/v2/Session/UploadFiles', headers=headers,files=files)
    print(r.text)
    return r.text

def check_api(jobid):
    data = {'JobID':jobid}
    json1 = json.dumps(data)
    response = requests.post('https://ai-interview.54ucl.com:3004/v2/Session/CheckFiles', headers={'Content-Type': 'application/json'},data=json1)
    print(response.text)
    return response.text

def converfile(filename,SessionID):
    ff = ffmpy.FFmpeg(
        inputs = {filename:'-y'},
        outputs = {'uploadfile/'+SessionID+'.mkv':None}
    )
    ff.run()

def Search_db():
    while 1:
        #session.flush() 
        AllData = session.query(Proxy).filter(and_(Proxy.conver==0,Proxy.files!='' , Proxy.SessionID!='',)).order_by(Proxy.id).first()
        conver_count = session.query(Proxy).filter(and_(Proxy.conver==1,Proxy.files!='' , Proxy.SessionID!='',)).count()
        #print(AllData)
        #len_all = session.query(Proxy).count()
        print(conver_count)
        session.close()
        if AllData!=None and  os.path.isfile(AllData.files)==True:
        #if AllData!=None and  os.path.isfile(AllData.files) and conver_count<5:
            db_id = AllData.id
            SessionID = AllData.SessionID
            files = AllData.files
            AllData.conver=1
            session.add(AllData)
            session.commit()

            try:
            	subprocess.Popen(['python3','test_conver.py',files,SessionID])
            except ffmpy.FFRuntimeError as e:
                
                print(e)
                pass
            #AllData.conver=1
            #session.add(AllData)
            #session.commit()
        elif AllData!=None and os.path.isfile(AllData.files)== False:
             AllData.conver = -1
             session.add(AllData)
             session.commit()                             
        time.sleep(3)
        '''non_upload = session.query(Proxy).filter(Proxy.upload==0,Proxy.conver==2,Proxy.files!='' , Proxy.SessionID!='').order_by(Proxy.id).first()
        #print(non_upload.id)
        try:
            if non_upload!=None and  os.path.isfile(non_upload.files):   
               print(non_upload.id)
               Api_id = json.loads(requests_api())['JobID']
               SessionID = non_upload.SessionID     
               files = non_upload.files
               #datasize = os.path.getsize(files)
               #subprocess.call()
               time.sleep(3)                             
               upload_api(SessionID,Api_id)
               time.sleep(2)
               check_api(Api_id)
               non_upload.upload=1
               session.add(non_upload)
               session.commit()
        except Exception as e:
            print(e)
            #err_conver.append(AllData[i].SessionID)
            pass
        time.sleep(2)'''

def check_Alive():
    while 1:
        print(t.is_alive())
        if (t.is_alive() == False):
          global t
          t = threading.Thread(target = Search_db,)
          t.daemon = True
          t.start()
        time.sleep(5) 

if __name__ == '__main__':
    global t
    t = threading.Thread(target = Search_db,)
    t.daemon = True
    t.start()
    alive = threading.Thread(target = check_Alive,)
    alive.daemon = True
    alive.start()
    #app.run(host='0.0.0.0',port=4000,ssl_context=('bundle.crt','private.key'))
    app.run(debug=True,host='0.0.0.0',port=4000)
    
